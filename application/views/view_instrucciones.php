<?php
require('fpdf18/fpdf.php');

//$data=array($_GET["prof"],$_GET["po"],$_GET["quan"],$_GET["qual"]);

class PDF extends FPDF
{
    function SetDash($black=false, $white=false)
    {
        if($black and $white)
            $s=sprintf('[%.3f %.3f] 0 d', $black*$this->k, $white*$this->k);
        else
            $s='[] 0 d';
        $this->_out($s);
    }
}


// Instanciation of inherited class
$pdf = new PDF();
$pdf-> AddPage();

$pdf->Image('http://localhost/samnori-1/HCJframework/images/nr2.jpg',7,7,17);

        
if(isset($records)){
                  $quan=$this->input->get('quan');      
    foreach($records as $row){
        
        
        $id=$row->item.'-'.$row->thickness.'X'.$row->width.'X'.$row->length;
        
     if($id==$this->input->get('id')){ 
          $thi=$row->thickness* 0.039370;
         if($row->length<11){
           $h= round(29/$thi);
         }
         else{
            $h= round(22/$thi);
         }
         $wid=$row->width;
         $w= floor(49/$wid);
         $sticker=$w*$wid;
         $unit=($h*$w);
         $totalUnits=round($quan/$unit);
         $msg="";
         
      $pdf->SetFont('Arial','B',15);     
      $pdf->Ln(10); 
      $pdf->Cell(6);
      $pdf->Cell(183,12,'Profile Instructions ',0,0,'C');  
         $pdf->Ln(); 
    
       $pdf->Cell(6);
      $pdf->SetFont('Arial','B',14); 
      $pdf->Cell(183,12,'Moulder Area',0,0,'L');      
         
      $pdf->SetFont('Arial','I',12);
      $pdf->Ln();
      $pdf->Cell(14);
      $pdf->Cell(43,6,'Item: '.$row->item,0,0,'L');
      $pdf->Cell(7);
      $pdf->Cell(60,6,'Description: '.$row->thickness.'mm X '.$row->width.'" X '.$row->length.'"',0,0,'L');
         
         $pdf->SetFont('Arial','B',14);     
      $pdf->Ln(); 
             
         $pdf->SetFont('Arial','B',14);     
      $pdf->Ln(); 
         
      $pdf->Cell(6);
      $pdf->Cell(183,12,'Rip Saw Area',0,0,'L');  
      $pdf->SetFont('Arial','I',12);
             
         $two=($this->input->get('quan'))*(.02);
          
         $pdf->Ln();
      $pdf->Cell(6);
         
          $pdf->SetFont('Arial','B',13); 
      
      $pdf->Cell(14);
      $pdf->Cell(36,10,'Each rip size: ',0,0,'L');
         $pdf->SetFont('Arial','I',13); 
      $pdf->Cell(3);
      $pdf->Cell(12,10, ($wid+.064),0,0,'L');
       $pdf->Cell(3);
      $pdf->Cell(30,10,' inches.',0,0,'L');   
       $pdf->Ln();  
         
         $xwid=$wid+.064;
         $piezas=floor(50/($xwid));
         $cantidad=$piezas*($xwid);
         $drop= 50-$cantidad;
         $sheets=ceil($quan/$piezas);
         
          $xwid3=$wid+.064;
         $piezas3=floor(49/($xwid3));
         $cantidad3=$piezas3*($xwid3);
         $drop3= 49-$cantidad3;
         $sheets3=ceil($quan/$piezas3);
          
         $pdf->SetFont('Arial','I',13); 
      $pdf->Cell(14);
      $pdf->Cell(54,3,' 49" SHEET:         '.$piezas3.' rips.',0,0,'L');
        
         $pdf->SetFont('Arial','B',13);
        $pdf->Cell(3);
      $pdf->Cell(11,3,'Drop:',0,0,'L');
      $pdf->Cell(3);
         
        $pdf->SetFont('Arial','I',13); 
      $pdf->Cell(28,3, $drop3.' inches.',0,0,'L');
         
          $pdf->SetFont('Arial','B',13);
        $pdf->Cell(3);
      $pdf->Cell(17,3,'Sheets:',0,0,'L');
      $pdf->Cell(3);
         
        $pdf->SetFont('Arial','I',13); 
      $pdf->Cell(24,3, $sheets3.' sheets.',0,0,'L');
         $pdf->Ln(); 
         
         
      $pdf->Ln();
      $pdf->Cell(14);
      $pdf->Cell(54,3,' 50" SHEET:         '.$piezas.' rips.',0,0,'L');
        
         $pdf->SetFont('Arial','B',13);
        $pdf->Cell(3);
      $pdf->Cell(11,3,'Drop:',0,0,'L');
      $pdf->Cell(3);
         
        $pdf->SetFont('Arial','I',13); 
      $pdf->Cell(28,3, $drop.' inches.',0,0,'L');
         
          $pdf->SetFont('Arial','B',13);
        $pdf->Cell(3);
      $pdf->Cell(17,3,'Sheets:',0,0,'L');
      $pdf->Cell(3);
         
        $pdf->SetFont('Arial','I',13); 
      $pdf->Cell(24,3, $sheets.' sheets.',0,0,'L');
         
       
         $pdf->Ln();  
         
         $xwid2=$wid+.064;
         $piezas2=floor(61.5/($xwid2));
         $cantidad2=$piezas2*($xwid2);
         $drop2= 61.5-$cantidad2;
         $sheets2=ceil($quan/$piezas2);
         
          $pdf->SetFont('Arial','I',13); 
      $pdf->Ln();
      $pdf->Cell(14);
      $pdf->Cell(54,3,' 61 1/2" SHEET:   '.$piezas2.' rips.',0,0,'L');
        
         $pdf->SetFont('Arial','B',13);
        $pdf->Cell(3);
      $pdf->Cell(11,3,'Drop:',0,0,'L');
      $pdf->Cell(3);
         
        $pdf->SetFont('Arial','I',13); 
      $pdf->Cell(28,3, $drop2.' inches.',0,0,'L');
         
          $pdf->SetFont('Arial','B',13);
        $pdf->Cell(3);
      $pdf->Cell(17,3,'Sheets:',0,0,'L');
      $pdf->Cell(3);
         
        $pdf->SetFont('Arial','I',13); 
      $pdf->Cell(24,3, $sheets2.' sheets.',0,0,'L');
         $pdf->Ln();  
         $pdf->Ln();          
      $pdf->Cell(14);
      $pdf->Cell(143,16,'Finish Rip: '.$quan.' pcs. add extra pieces for setup and rejects.',0,0,'L');
    
         
      $pdf->Ln();
      $pdf->SetFont('Arial','B',14);
      $pdf->Cell(6);
      $pdf->Cell(183,12,'Stacking & Packaging Area',0,0,'L');  
                 
         if(($unit*$totalUnits)!=$quan){
             
             $p=$unit/2;
              $xpieces=$quan-($unit*$totalUnits);
             
             if($xpieces<0){
                $xpieces= $xpieces+$unit;
             }
             
             if($xpieces>$p){
                $msg="The last unit will have a minimum of ".$xpieces." pcs.";
             }
             else{
                 if(round($totalUnits)==1){
                $msg="Add a minimum of ".$xpieces." pcs. to the unit";
                 }
                 else{
                  $msg="Add a minimum of ".$xpieces." pcs. to the last unit";
                 }
             }
         }
         
        $pdf->SetFont('Arial','B',13); 
      $pdf->Ln();
      $pdf->Cell(14);
      $pdf->Cell(36,6,'width x height: ',0,0,'L');
      
      
        $pdf->SetFont('Arial','I',13); 
         $pdf->Cell(33,6,$w.' pcs. X '.$h.' pcs. ',0,0,'L');
         $pdf->Cell(7);
      $pdf->Cell(60,6,'*Pieces per Unit: '.$unit,0,0,'L');
      $pdf->Ln();
      $pdf->Cell(14);
      $pdf->Cell(43,6,'Sticker Length: '.$sticker.'"',0,0,'L');
        
         if(round($totalUnits)==1){
             $s="unit";         
         }
         else{
             $s="units"; 
         }
          $pdf->Ln();
         $pdf->Ln();
       $pdf->Cell(86);
       $pdf->Cell(78,4,$msg,0,0,'R');
         $pdf->Ln();
       $pdf->Ln(5);
       $pdf->Cell(86);
       $pdf->Cell(78,4,'Have a total of: '.round($totalUnits).' '.$s,0,0,'R');
        
         
         
         
         
         
         
         
      
      
     }
    }
}
else{
$pdf->Cell(70,13,'nada',0,0,'R');
}

$pdf->Output();



?>