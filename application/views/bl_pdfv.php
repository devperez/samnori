<?php
require('fpdf18/fpdf.php');

class PDF extends FPDF
{
    function SetDash($black=false, $white=false)
    {
        if($black and $white)
            $s=sprintf('[%.3f %.3f] 0 d', $black*$this->k, $white*$this->k);
        else
            $s='[] 0 d';
        $this->_out($s);
    }
}


// Instanciation of inherited class
$pdf = new PDF();
$pdf-> AddPage();
$pdf->Ln(6);

$pdf->Image('http://localhost/samnori-1/HCJframework/images/nr2.jpg',57,15,37);
$pdf->SetFont('Arial','B',20);  
 $pdf->Cell(150);
$pdf->Cell(20,6,'BILL OF LADING',0,0,'C');
$pdf->Ln();
$pdf->Cell(20,1,'',0,0,'C');
$pdf->Ln();
$pdf->Cell(60);
$pdf->SetFont('Arial','',13);  
$pdf->Cell(20,9,'NORTH RIVER MILLWORK LLC ',0,0,'C');  
$pdf->Cell(60);
$pdf->SetFont('Arial','',11);  
$pdf->Cell(28,10,'DATE',1,0,'C'); 
$pdf->Cell(20,10,'BOL #',1,0,'C');
$pdf->Ln();
$pdf->Cell(60);
  $pdf->SetFont('Arial','',13);  
$pdf->Cell(20,0,'PO BOX 5864 ',0,0,'C');

$pdf->Ln();
$pdf->Cell(60);
 $pdf->SetFont('Arial','',13);  
$pdf->Cell(20,8,'AUBURN, CA. 95604',0,0,'C'); 
$pdf->Cell(60);
$pdf->SetFont('Arial','',11);  
$pdf->Cell(28,8, $this->input->get('date'),1,0,'C');  
$pdf->Cell(20,8, date("mdY"),1,0,'C');

$pdf->Ln();
$pdf->Ln();

$pdf->Cell(10);
$pdf->SetFont('Arial','',11);  
$pdf->Cell(85,8,'CARRIER INFORMATION',1,0,'L'); 
$pdf->Cell(6);  
$pdf->Cell(89,8,'CONSIGHNED TO',1,0,'C');
$pdf->Ln();
$pdf->Cell(10);
$pdf->SetFont('Arial','',11);  
$pdf->Cell(85,15,$this->input->get('carrier'),1,0,'L');
$pdf->Cell(6);
$pdf->Cell(89,20," ",1,0,'R');
if(isset($records2)){
    foreach($records2 as $row2){
        $id=''.$row2->nombre.', '.$row2->direccion.', '.$row2->ciudad;

        if($id==$this->input->get('consi')){
            $pdf->SetFont('Arial','',12);
            $pdf->Cell(-89);
            $pdf->Cell(0,5,$row2->nombre,0,0,'C');
            $pdf->Ln();
            $pdf->Cell(101);
            $pdf->Cell(89,5,$row2->direccion,0,0,'C');
            $pdf->Ln();
            $pdf->Cell(101);
            $pdf->Cell(89,5,$row2->ciudad,0,0,'C');
            $pdf->Ln();
            $pdf->Cell(101);
            $pdf->Cell(89,5,$row2->codigopostal,0,0,'C');
        }
    }
}
$pdf->Ln();

$pdf->Cell(10);
$pdf->SetFont('Arial','',11);  
$pdf->Cell(32,8,'LOADED AT',1,0,'C'); 
$pdf->Cell(30,8,'FOB',1,0,'C');
$pdf->Cell(36);
 $pdf->SetFont('Arial','B',11); 
$pdf->Cell(82,8,'DESTINATION',1,0,'L');

$pdf->Ln();
$pdf->Cell(10);
$pdf->SetFont('Arial','I',11);  

$pdf->Cell(32,8,'STOCKTON',1,0,'C');
$pdf->Cell(30,8,'MILL',1,0,'C');
$pdf->Cell(36);
$pdf->Cell(82,20," ",1,0,'R');
if(isset($records2)){

    foreach($records2 as $row2){
        $id=''.$row2->nombre.', '.$row2->direccion.', '.$row2->ciudad;
        if($id==$this->input->get('destination')){
            $pdf->SetFont('Arial','',12);
            $pdf->Cell(-81);
            $pdf->Cell(0,5,$row2->nombre,0,0,'L');
            $pdf->Ln();
            $pdf->Cell(109);
            $pdf->Cell(82,5,$row2->direccion,0,0,'L');
            $pdf->Ln();
            $pdf->Cell(109);
            $pdf->Cell(82,5,$row2->ciudad,0,0,'L');
            $pdf->Ln();
            $pdf->Cell(109);
            $pdf->Cell(82,5,$row2->codigopostal,0,0,'L');
        }
    }
}

$pdf->Ln();
$pdf->Cell(35,2,'',0,0,'C'); 
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(10);
$pdf->SetFont('Arial','',11);  
$pdf->Cell(35,8,'Item',1,0,'C'); 
$pdf->Cell(85,8,'Description',1,0,'C'); 
$pdf->Cell(35,8,'Shipped',1,0,'C'); 
$pdf->Cell(25,8,'UM',1,0,'C'); 



 
    $aux=10;
if(isset($records)){
                    
    foreach($records as $row){
        
        
        $id=$row->item.' - '.$row->thickness.' X '.$row->width.' X '.$row->length;
        
     if($id==$this->input->get('item')){

         $pdf->SetFont('Arial','',12);
         $pdf->Ln();
         $pdf->Cell(10);
         $pdf->Cell(35,8,' '.$row->item,1,0,'C');
         $pdf->Cell(85,8,' '.$row->thickness.'mm X '.$row->width.'" X '.$row->length.'"',1,0,'C');
         $pdf->Cell(35,8,$this->input->get('shipped'),1,0,'L');
         $pdf->Cell(25,8,$this->input->get('um'),1,0,'L');
       $aux--;
     }
        if($id==$this->input->get('item2')){

            $pdf->SetFont('Arial','',12);
            $pdf->Ln();
            $pdf->Cell(10);
            $pdf->Cell(35,8,' '.$row->item,1,0,'C');
            $pdf->Cell(85,8,' '.$row->thickness.'mm X '.$row->width.'" X '.$row->length.'"',1,0,'C');
            $pdf->Cell(35,8,$this->input->get('shipped2'),1,0,'L');
            $pdf->Cell(25,8,$this->input->get('um2'),1,0,'L');
            $aux--;
        }
        if($id==$this->input->get('item3')){

            $pdf->SetFont('Arial','',12);
            $pdf->Ln();
            $pdf->Cell(10);
            $pdf->Cell(35,8,' '.$row->item,1,0,'C');
            $pdf->Cell(85,8,' '.$row->thickness.'mm X '.$row->width.'" X '.$row->length.'"',1,0,'C');
            $pdf->Cell(35,8,$this->input->get('shipped3'),1,0,'L');
            $pdf->Cell(25,8,$this->input->get('um3'),1,0,'L');
            $aux--;
        }
        if($id==$this->input->get('item4')){

            $pdf->SetFont('Arial','',12);
            $pdf->Ln();
            $pdf->Cell(10);
            $pdf->Cell(35,8,' '.$row->item,1,0,'C');
            $pdf->Cell(85,8,' '.$row->thickness.'mm X '.$row->width.'" X '.$row->length.'"',1,0,'C');
            $pdf->Cell(35,8,$this->input->get('shipped4'),1,0,'L');
            $pdf->Cell(25,8,$this->input->get('um4'),1,0,'L');
            $aux--;
        }
        if($id==$this->input->get('item5')){

            $pdf->SetFont('Arial','',12);
            $pdf->Ln();
            $pdf->Cell(10);
            $pdf->Cell(35,8,' '.$row->item,1,0,'C');
            $pdf->Cell(85,8,' '.$row->thickness.'mm X '.$row->width.'" X '.$row->length.'"',1,0,'C');
            $pdf->Cell(35,8,$this->input->get('shipped5'),1,0,'L');
            $pdf->Cell(25,8,$this->input->get('um5'),1,0,'L');
            $aux--;
        }
        if($id==$this->input->get('item6')){

            $pdf->SetFont('Arial','',12);
            $pdf->Ln();
            $pdf->Cell(10);
            $pdf->Cell(35,8,' '.$row->item,1,0,'C');
            $pdf->Cell(85,8,' '.$row->thickness.'mm X '.$row->width.'" X '.$row->length.'"',1,0,'C');
            $pdf->Cell(35,8,$this->input->get('shipped6'),1,0,'L');
            $pdf->Cell(25,8,$this->input->get('um6'),1,0,'L');
            $aux--;
        }
        if($id==$this->input->get('item7')){

            $pdf->SetFont('Arial','',12);
            $pdf->Ln();
            $pdf->Cell(10);
            $pdf->Cell(35,8,' '.$row->item,1,0,'C');
            $pdf->Cell(85,8,' '.$row->thickness.'mm X '.$row->width.'" X '.$row->length.'"',1,0,'C');
            $pdf->Cell(35,8,$this->input->get('shipped7'),1,0,'L');
            $pdf->Cell(25,8,$this->input->get('um7'),1,0,'L');
            $aux--;
        }
       
    }
}
else{
$pdf->Cell(70,13,'nada',0,0,'R');
}

for($x=0;$x<$aux;$x++){
     $pdf->Ln(); 
      $pdf->Cell(10); 
      $pdf->Cell(35,8,' ',1,0,'C');
      $pdf->Cell(85,8,' ',1,0,'C'); 
$pdf->Cell(35,8,'',1,0,'L'); 
$pdf->Cell(25,8,'',1,0,'L'); 

}
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(10); 
$pdf->Cell(90,20,'SHIPPER Signature____________________',1,0,'L'); 
$pdf->Cell(90,20,'CARRIER Signature____________________',0,0,'L'); 
$pdf->Ln();
$pdf->Cell(10); 
$pdf->Cell(90,12,'ALL LOADS MUST BE TARPED',0,0,'c'); 
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(30); 
$pdf->Cell(40,6,'Phone #',1,0,'C'); 
$pdf->Cell(30); 
$pdf->Cell(80,6,'Web Site',1,0,'C'); 
$pdf->Ln();
$pdf->Cell(30); 
$pdf->Cell(40,6,'(530) 305-7660',1,0,'C'); 
$pdf->Cell(30); 
$pdf->Cell(80,6,'WWW.NORTHRIVERMILLWORK.COM',1,0,'C'); 
$pdf->Ln();



$pdf->Output();



?>