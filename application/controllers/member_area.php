<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_area extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');     
    } 
	public function index()
	{
		 $is_logged_in=$this->session->userdata('is_logged_in');
          if(!isset($is_logged_in)||$is_logged_in !=true){
              $data['main_content']='no_permission';
           $this->load->view('includes/template.php',$data);
          
         }
         else{
           $data['main_content']='member_area';
           $this->load->view('includes/templateMember.php',$data);
         }
	}
    function add_order(){
            $data=array();
        $table="profiles";
              if($query=$this->membership_model->get_records($table)){
                   $data['records']=$query;
              }
                $this->load->view('includes/headerMember'); 
               $this->load->view("add_order",$data);
    
    }
    function add_userc(){
            $data=array();
        $table="clientes";
              if($query=$this->membership_model->get_records($table)){
                   $data['records']=$query;
              }
                $this->load->view('includes/headerMember'); 
               $this->load->view("add_user",$data);
    
    }
    function create_tagsc(){
        
        $this->load->view("tags_pdfv"); 
    
    }
     function pdf_blc(){

         $data=array();
         $data2=array();
         $allData=array();
         $table="profiles";
         $table2="clientes";
         if($query=$this->membership_model->get_records($table)){
             $data['records']=$query;
             if($query2=$this->membership_model->get_records($table2)){
                 $data2['records2']=$query2;
                 $allData=(object)array_merge((array)$data, (array)$data2);
             }else{
                 $allData=$data['records'];
             }
         }
               $this->load->view("bl_pdfv",$allData);
    
    }
         function create_instruccionesc(){
        $data=array();
        $table="profiles";    
              if($query=$this->membership_model->get_records($table))
              {
                   $data['records']=$query;
              }
               $this->load->view("view_instrucciones",$data);
        }
    function view_profiles(){
            $data=array();
        $table="profiles";
              if($query=$this->membership_model->get_records($table)){
                   $data['records']=$query;
              }
                $this->load->view('includes/headerMember'); 
               $this->load->view("view_profiles",$data);
    
    }
    function view_users(){
            $data=array();
            $table="usuario";
              if($query=$this->membership_model->get_records($table)){
                   $data['records']=$query;
              }
                $this->load->view('includes/headerMember'); 
               $this->load->view("view_users",$data);
    
    }
     function create_tags(){
            $data=array();
        $table="profiles";
              if($query=$this->membership_model->get_records($table)){
                   $data['records']=$query;
              }
                $this->load->view('includes/headerMember'); 
               $this->load->view("create_tagsv",$data);
    
    }
    
    function create_billladingc(){
        $data=array();
        $data2=array();
        $allData=array();
        $table="profiles";
        $table2="clientes";
              if($query=$this->membership_model->get_records($table)){
                   $data['records']=$query;
                  if($query2=$this->membership_model->get_records($table2)){
                      $data2['records2']=$query2;
                      $allData=(object)array_merge((array)$data, (array)$data2);
                  }else{
                      $allData=$data['records'];
                      echo '<script type="text/javascript">console.log("hello!");</script>';
                  }
              }

                $this->load->view('includes/headerMember'); 
               $this->load->view("create_billladingv",$allData);
    
    }
      

    function view_inventory(){
            $data=array();
            $table="warehouse";
              if($query=$this->membership_model->get_records($table)){
                   $data['records']=$query;
              }
                $this->load->view('includes/headerMember'); 
                $this->load->view("view_inventory",$data);
    
    }
     function add_warehousec(){
            $data=array();
            $table="clientes";
              if($query=$this->membership_model->get_records($table)){
                   $data['records']=$query;
              }
                $this->load->view('includes/headerMember'); 
                $this->load->view("add_warehouse",$data);
    
    }
    function delete_client(){
        
            $table="clientes";
            $this->membership_model->delete($table);
               
        $this->update_client();
    }
    function delete_warehouse(){
        
            $table="warehouse";
            $this->membership_model->delete($table);
               
        $this->update_warehousec();
    }
    function delete_user(){
        
            $table="usuario";
             $this->membership_model->delete($table);
               
        $this->view_users();
    }
    function delete_profile(){
        
            $table="profiles";
             $this->membership_model->delete($table);
               
        $this->view_profiles();
    }
    function adtotab(){
        
       if($this->input->post('table')){
         $table=$this->input->post('table');
       } 
        else{
           $table=$this->input->get('tab');
        }
        
        switch ($table){
            case "clientes":
                if($this->input->post('nombre')==''||$this->input->post('correo')==''||$this->input->post('direccion')==''||$this->input->post('codigopostal')==''){
              $data['main_content']='invalid';
              $this->load->view('includes/templateMember.php',$data); 
        
        }
        else{
         $data=array(
             'nombre'=>$this->input->post('nombre'),
              'correo'=>$this->input->post('correo'),
              'direccion'=>$this->input->post('direccion'),
              'codigopostal'=>$this->input->post('codigopostal'),
              'ciudad'=>$this->input->post('ciudad'),
              'estado'=>$this->input->post('estado')
          );
         $query=$this->membership_model->add_to_table($data,$table);
            $data['main_content']='valid';
              $this->load->view('includes/templateMember.php',$data);  
        
          }
                break;
        case "profiles":  
               
         $data=array(
             'item'=>$this->input->post('item'),
              'thickness'=>$this->input->post('thi'),
             'width'=>$this->input->post('wid'),
             'length'=>$this->input->post('len')
          );
         $query=$this->membership_model->add_to_table($data,$table);
            $data['main_content']='valid';
              $this->load->view('includes/templateMember.php',$data);  
        
          
                break;
            case "orders": if($this->input->post('item')==''||$this->input->post('quanity')==''){
              $data['main_content']='invalid';
              $this->load->view('includes/templateMember.php',$data); 
        
        }
        else{
         $data=array(
             'item'=>$this->input->post('item'),
              'quanity'=>$this->input->post('quanity')
          );
         $query=$this->membership_model->add_to_table($data,$table);
            $data['main_content']='valid';
              $this->load->view('includes/templateMember.php',$data);  
        
          }
                break;
            case "warehouse": 
                $data=array(
                'item'=>$this->input->get('item'),
              'thickness'=>$this->input->get('thi'),
             'width'=>$this->input->get('wid'),
             'length'=>$this->input->get('len'),
             'quanity'=>$this->input->get('quan'),  
             'owner'=>$this->input->get('owner')  
          );
         $query=$this->membership_model->add_to_table($data,$table);
            $data['main_content']='valid';
              $this->load->view('includes/templateMember.php',$data);  
        
                break;
                
            case "usuario": if($this->input->get('nom')==''||$this->input->get('ema')==''||$this->input->get('pas')==''){
              $data['main_content']='invalid';
              $this->load->view('includes/templateMember.php',$data); 
        }
        else{   
            
            switch ($this->input->get('area')) {
                     case "Administrator":
                               $area=0;
                        break;
                      case "Employee":
                               $area=1;
                       break;
                      case "Customer":
                               $area=3;
                      break;
            } 
       
            
             $data=array(
              'id'=>'',
              'nombre'=>$this->input->get('nom'),
              'apellidos'=>$this->input->get('ape'),
              'nom_usuario'=>$this->input->get('nom'),
              'password'=>md5($this->input->get('pas')),
              'correo'=>$this->input->get('ema'),
              'area'=>$area,
                'company'=>$this->input->get('company'),
             );
           $query=$this->membership_model->add_to_table($data,$table);
           
            $data['main_content']='valid';
           $this->load->view('includes/templateMember.php',$data);  
        
          }
                break;        
        
        }
        
       
    }
     function update_warehousec(){
            $data=array();
            $table="warehouse";
              if($query=$this->membership_model->get_records($table)){
                   $data['records']=$query;
              }
                $this->load->view('includes/headerMember'); 
               $this->load->view("update_warehousev",$data);
    
    }
    function updated_warehouse(){
        $table="warehouse";
        $msg=$_GET["msg"];
    $data=array(
             'item'=>$_GET["item"],
             'thickness'=>$_GET["thi"],
             'width'=>$_GET["wid"],
              'length'=>$_GET["len"],        
              'quanity'=>$_GET["quan"]
          );
         $query=$this->membership_model->update($data,$table, $_GET["id"]);
        
        if($msg!=""){
        $this->load->library('email');

$this->email->from('samnori', 'Administrador');
$this->email->to('hubermouldings@att.net'); 
$this->email->cc('another@another-example.com'); 
$this->email->bcc('them@their-example.com'); 

$this->email->subject('Warehouse');
$this->email->message($msg);	
$this->email->send();
 
        }
            $this->update_warehousec();  
    
    }
    
    function update_client(){
            $data=array();
            $table="clientes";
              if($query=$this->membership_model->get_records($table)){
                   $data['records']=$query;
              }
                $this->load->view('includes/headerMember'); 
               $this->load->view("update_client",$data);
    
    }
    function updated_client(){
        $table="clientes";
    $data=array(
             'nombre'=>$_GET["nombre"],
             'correo'=>$_GET["correo"],
              'direccion'=>$_GET["direccion"],
             'codigopostal'=>$_GET["codigopostal"],
               'ciudad'=>$_GET["ciudad"],
                'estado'=>$_GET["estado"]
          );
         $query=$this->membership_model->update($data,$table, $_GET["id"]);
            $this->update_client();  
    
    }
    
         
}