<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	
	public function index()
	{
        
        $this->session->sess_destroy();
        
        if(!isset($is_logged_in)||$is_logged_in !=true){
        $data['main_content']='login_form';
        $this->load->view('includes/template.php',$data);
        }
           else{
		
           }
	}
    
    function validate_credentials()
    {
      $this->load->model('membership_model');
      $query=$this->membership_model->validate();
       
        
        if($query){
            $data=array(
               'username'=>$this->input->post('username'),
                'area'=>$this->membership_model->user_area(),
                'company'=>$this->membership_model->user_company(),
                'is_logged_in'=>true
            );
        $this->session->set_userdata($data);
            setcookie('area', $this->membership_model->user_area(), time() + 4800);
            redirect('member_area');
        
        }
         else{
           // redirect($query."fg");
             
             redirect('');
         }
    }
   
}
